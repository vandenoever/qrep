import QtQuick 2.0
import QtQuick.Controls 1.1
import QtQuick.Layouts 1.0
import QtQuick.Window 2.0
import RustCode 1.0

Window {
    visible: true
    width: 600
    height: 800
    title: qsTr("File finder")

    Grep {
        id: grep
        query: query.text
    }

    ColumnLayout {
        anchors.fill: parent
        RowLayout {
            Layout.fillWidth: true
            TextField {
                id: query
                Layout.fillWidth: true
                focus: true
                placeholderText: "Search"
            }
            BusyIndicator {
                running: grep.busy
                visible: grep.busy
                Layout.preferredWidth: query.height
                Layout.preferredHeight: query.height
            }
        }

        ListView {
            Layout.fillWidth: true
            Layout.fillHeight: true
            model: grep
            clip: true
            delegate: Text {
                text: format(name, line)
                textFormat: Text.StyledText
                wrapMode: Text.WordWrap
                width: parent.width
                MouseArea {
                    anchors.fill: parent
                    onClicked: Qt.openUrlExternally("file:" + path)
                }
            }
        }
    }
    function format(name, line) {
        let s = "<u><font color='blue'>" + name + "</font></u>";
        if (line) {
            s += " " + line;
        }
        return s;
    }
}
