#include "Bindings.h"
#include <QtGui/QGuiApplication>
#include <QtQml/QQmlApplicationEngine>

extern "C" {
    int main_cpp(const char* appPath);
}

int main_cpp(const char* appPath) {
    int argc = 1;
    char* argv[1] = { (char*)appPath };
    QGuiApplication app(argc, argv);
    qmlRegisterType<Grep>("RustCode", 1, 0, "Grep");
    QQmlApplicationEngine engine;
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    return app.exec();
}
