extern crate grep_matcher;
extern crate grep_regex;
extern crate grep_searcher;
extern crate htmlescape;
extern crate libc;
extern crate walkdir;
extern crate spmc;

mod implementation;

pub mod interface {
    include!(concat!(env!("OUT_DIR"), "/src/interface.rs"));
}

use std::os::raw::{c_char, c_int};
extern "C" {
    fn main_cpp(app: *const c_char) -> c_int;
}

fn main() {
    use std::ffi::CString;
    let mut args = ::std::env::args();
    let app = CString::new(args.next().unwrap()).unwrap();
    unsafe {
        main_cpp(app.as_ptr());
    }
}
